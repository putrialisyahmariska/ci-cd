const express = require("express"); // import express
const router = express.Router(); // import router
const passport = require("passport"); // import passport
const auth = require("../middlewares/auth"); // import passport auth strategy
const UserController = require("../controllers/userController"); // import userController
const userValidator = require("../middlewares/validators/userValidator"); // import userValidator

const userController = require("../controllers/userController");

// if user go to localhost:3000/signup
router.post("/signup", [
	userValidator.signup,
	function (req, res, next) {
		passport.authenticate(
			"signup",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "Error",
						message: info.message,
					});
					return;
				}

				UserController.signup(user, req, res, next);
			}
		)(req, res, next);
	},
]);

// if user go to localhost:3000/login
router.post("/login", [
	userValidator.login,
	function (req, res, next) {
		passport.authenticate(
			"login",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "Error",
						message: info.message,
					});
					return;
				}

				UserController.login(user, req, res, next);
			}
		)(req, res, next);
	},
]);

router.put("/insertPP", [
	userValidator.insertPP,
	function (req, res, next) {
		passport.authenticate(
			"insertPP",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "Error",
						message: info.message,
					});
					return;
				}

				UserController.insertPP(user, req, res, next);
			}
		)(req, res, next);
	},
]);
//masih belum final
router.get("/getPP/:img", UserController.getPP);

router.put("/changeName", [
	userValidator.changeName,
	function (req, res, next) {
		passport.authenticate(
			"changeName",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "404",
						message: info.message,
					});
					return;
				}

				UserController.changeName(user, req, res, next);
			}
		)(req, res, next);
	},
]);

router.put("/changePass", [
	userValidator.changePass,
	function (req, res, next) {
		passport.authenticate(
			"changePass",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "404",
						message: info.message,
					});
					return;
				}

				UserController.changePass(user, req, res, next);
			}
		)(req, res, next);
	},
]);
module.exports = router; // export router
